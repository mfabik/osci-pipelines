spec:
  inputs:
    included_pipeline_ref:
      default: 'main'
---

.with_artifacts:
  artifacts:
    when: always
    reports:
      dotenv:
        - metadata.env  # Information to pass along as needed, e.g. build ID and link
    expire_in: 6 months

# Clone the pipeline repo to access scripts in the rog/scripts directory
.with_scripts:
  before_script:
    - |
      if [ $[[ inputs.included_pipeline_ref ]] = "main" ]; then
        echo "Production run, shallow-cloning pipeline repo"
        git clone --depth 1 https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines.git .rog-pipeline
      else
        echo "Testing MR run, full-cloning pipeline repo"
        git clone https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines.git .rog-pipeline
      fi
    - cd .rog-pipeline
    - git config advice.detachedHead false
    - |
      if [ $[[ inputs.included_pipeline_ref ]] != "main" ]; then
        echo "Fetching MR content..."
        git fetch origin "refs/$[[ inputs.included_pipeline_ref ]]:$[[ inputs.included_pipeline_ref ]]"
      fi
    - echo "Using ref $[[ inputs.included_pipeline_ref ]]."
    - git checkout $[[ inputs.included_pipeline_ref ]]
    - cd ..

# Template for handling retries for weird GitLab failures. This has nothing to do
# with retries for test execution issues.
.with_retries:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
      - unknown_failure
      - api_failure

.export_token: |
  if [[ "$RHEL_OR_STREAM" == "rhel" ]]; then
    GITLAB_TOKEN_FILENAME="gitlab-private.token"
  elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]]; then
    GITLAB_TOKEN_FILENAME="gitlab.token"
  fi
  export GITLAB_TOKEN=$(cat "/opt/secrets/$GITLAB_TOKEN_FILENAME")

.pick_build_options: |
  if [[ "$USE_DRAFTS" == "1" ]] ; then
    BUILD_TYPE="--draft"
  else
    # Current production use for RoG
    ARCH_OVERRIDE="--arch-override=x86_64"
    BUILD_TYPE="--scratch"
  fi

.check_if_rebase_needed:
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:590e052c"
  script:
    - |
      if [[ "$CI_COMMIT_SHA" != "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]] ; then
        echo "The MR is out of sync with the target branch, please rebase and push again."
        exit 1
      fi

.pipeline_init:
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:590e052c"
  extends: [.with_artifacts]
  artifacts:
    paths:
      - environment.log
  script:
    - !reference [.export_token]
    - export GLAB_CONFIG_DIR=/tmp
    - |
      if [ ! "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        HAS_CONFLICTS=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID} | jq ".has_conflicts")
        if [ "$HAS_CONFLICTS" = "false" ]; then
          echo "CI_MERGE_REQUEST_SOURCE_BRANCH_SHA undefined, falling back to CI_COMMIT_SHA"
          echo "This is a bug, proceed with caution and review the environment.log artifact file."
          glab mr note -m "<h4>CI_MERGE_REQUEST_SOURCE_BRANCH_SHA undefined, falling back to CI_COMMIT_SHA<br/>This is a bug, proceed with caution.<br/>Saving environment artifact for debugging.</h4>" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
        fi
        ROG_COMMIT_SHA=$CI_COMMIT_SHA
      else
        ROG_COMMIT_SHA=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      fi
    - echo "ROG_COMMIT_SHA=$ROG_COMMIT_SHA" >> metadata.env
    - echo "Checking side-tag"
    - |
      MR_JSON=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID})
    - |
      SIDE_TAG=$(echo $MR_JSON | jq -r '.description' | grep '^side-tag:' || :)
    - |
      if [ -n "$SIDE_TAG" ]; then
        SIDE_TAG=$(echo $SIDE_TAG | sed 's/^.*: \?//')  # Remove "side-tag: " prefix
      fi
      echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - |
      if [[ "$RHEL_OR_STREAM" == "rhel" ]]; then
        MR_COMMENT=""
        FORCE_LATEST_LABEL=""
      elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]]; then
        MR_COMMENT="<ol><li>Do not forget to review this MR carefully if it comes from an external contributor.</li><li>The downstream target MR label is set to <code>target::latest</code> by default if you don't set another target when creating the MR. If you want to use a different target, choose another of the allowed targets and start a new pipeline:</li><ul><li><code>target::zstream</code>: z-stream</li><li><code>target::exception</code>: exception</li><li>You can use the <code>/label</code> command in a comment to set the label.</li></ul></ol>"
        FORCE_LATEST_LABEL="true"
      fi
    - >
      if [ -n "$MR_COMMENT" ]; then
        glab mr note -m "$MR_COMMENT" --unique -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
      fi
    - >
      if [ -n "$FORCE_LATEST_LABEL" ] && [ -z "$CI_MERGE_REQUEST_LABELS" ]; then
        curl -X PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" --data '{"labels": "target::latest"}' https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID} > /dev/null
      fi
    - export | tee environment.log

.sync_mr_downstream:
  extends: [.with_artifacts, .with_retries]
  allow_failure: true
  needs:
    - job: pipeline_init
      optional: true
      artifacts: true
    - job: check_tickets
      optional: true
      artifacts: true
  image: images.paas.redhat.com/osci/base-minimal:901cda3c
  tags:
    - distrobaker
  script:
    - export
    - |
      if [[ "$RHEL_OR_STREAM" == "rhel" ]]; then
        VISIBILITY="private"
      elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]]; then
        VISIBILITY="public"
      fi
    - DISTROGITSYNC_URL="https://distrogitsync.osci.redhat.com/${VISIBILITY}_rpms/rpms/$CI_PROJECT_NAME/mr/$CI_MERGE_REQUEST_IID"
    - echo "$DISTROGITSYNC_URL"
    - CONTENT_LENGTH=$(curl --fail-with-body -X POST "$DISTROGITSYNC_URL" -i | grep "content-length" | tr -d -c 0-9)
    # If content length is zero something has gone wrong and we need to fail.
    - |
      if [ $CONTENT_LENGTH -eq 0 ] ; then
        echo "distrogitsync responded with zero-length response. Something is wrong, failing job."
        exit 1
      fi
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - echo "ROG_COMMIT_SHA=$ROG_COMMIT_SHA" >> metadata.env

.build_rpm:
  timeout: 24h
  allow_failure: true
  artifacts:
    paths:
      - distrobuildsync.html
    expose_as: 'Build Brew RPM log'
  extends: [.with_artifacts, .with_retries, .with_scripts]
  needs:
    - job: sync_mr_downstream
      artifacts: true
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/distrobuildsync:8e34b66e"
  script:
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - echo "ROG_COMMIT_SHA=$ROG_COMMIT_SHA" >> metadata.env
    - echo "SIDE_TAG=$SIDE_TAG"
    - kinit -k -t "/opt/secrets/distrobaker.keytab" "distrobaker/distrobaker.osci.redhat.com@REDHAT.COM"
    - !reference [.export_token]
    - !reference [.pick_build_options]
    - |
      # Get the build target.
      if [[ "$RHEL_OR_STREAM" == "rhel" ]]; then
        # Find leading digit(s) - RHEL major version
        X=$(grep -o '[0-9]\{1,2\}' <<< $CI_MERGE_REQUEST_TARGET_BRANCH_NAME | head -1)
        PROFILE="c${X}s"
        TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-z-candidate
      elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]]; then
        PROFILE=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      fi
    - |
      if [ -n "$SIDE_TAG" ]; then
        set +o pipefail
        DOWNSTREAM_SIDE_TAG=$(koji -p stream taginfo $SIDE_TAG | grep 'downstream_sidetag' | sed "s/.* '//;s/'//")
        if [ -z "$DOWNSTREAM_SIDE_TAG" ]; then
          echo "No downstream sidetag for $SIDE_TAG, exiting"
          exit 1 # TODO: Wait instead?
        fi
        ./.rog-pipeline/rog/scripts/wait-side-tag-sync.py $SIDE_TAG $DOWNSTREAM_SIDE_TAG
        TARGET=$DOWNSTREAM_SIDE_TAG
      fi
    - NS_COMPONENT="rpms/$CI_PROJECT_NAME"
    - IFS=',' read -r -a labels <<< "$CI_MERGE_REQUEST_LABELS"
    - |
      for label in "${labels[@]}"
      do
          if [[ $label == target::* ]]; then
              TARGET_LABEL="${label#target::}"
              break
          fi
      done
    - |
      if [ -z "$TARGET_LABEL" ]; then
        echo "Warning: MR label not set, using default 'target::latest'"
      fi
    - echo "Submitting brew build of ${NS_COMPONENT}#${ROG_COMMIT_SHA}, using distrobuildsync profile $PROFILE, target $TARGET, target label $TARGET_LABEL, build type $BUILD_TYPE"
    # TODO: Remove the -l debug once we know it works as expected
    # The -t and -p are used to specify build target.
    # If $TARGET is non-empty, it's used.
    # If $TARGET is not specified (empty) distrobuildsync uses target specified in the distrobaker config for the profile (-p $PROFILE).
    - python3 /tmp/distrobuildsync $BUILD_TYPE -a x86_64 -l debug -b "${NS_COMPONENT}#${ROG_COMMIT_SHA}" -t "$TARGET" -p "$PROFILE" -i "${TARGET_LABEL:-latest}" https://gitlab.cee.redhat.com/osci/distrobaker_config.git#rhel9 2>&1 | tee distrobuildsync.log
    - echo "<html><body><h1>Distrobuildsync build log</h1><pre>" > distrobuildsync.html
    - cat distrobuildsync.log >> distrobuildsync.html
    - echo "</pre></body></html>" >> distrobuildsync.html
    # : INFO : Build submitted for rpms/cups, target rhel-9.5.0-candidate; task 60118525; SCMURL: git+https://pkgs.devel.redhat.com/git/rpms/cups#4aa5ef7...
    - TASK_ID=$(tail distrobuildsync.log | grep "Build submitted for " | grep -Eo ' task [0-9]+' | awk '{ print $2 }')
    - TARGET_SUBMITTED=$(tail distrobuildsync.log | grep 'Build submitted for ' | grep -Eo ' target [^;]+' | awk '{ print $2 }')
    - |
      if [ "$SIDE_TAG" ]; then
        NVR=$(koji -p stream --quiet list-builds --package="$CI_PROJECT_NAME" --reverse | head -n 1 | cut -d ' ' -f 1)
        PATTERN=$(echo $NVR | sed 's/\./\\./g')
        set +o pipefail
        ADDITIONAL_ARTIFACTS=$(koji -p stream list-tagged --quiet --latest $SIDE_TAG | cut -d ' ' -f 1 | grep -v "$PATTERN" | sed -z 's/\n/,/g')
      fi
    - echo "KOJI_TASK_ID=$TASK_ID" >> metadata.env
    - echo "KOJI_BUILD_TARGET=$TARGET_SUBMITTED" >> metadata.env
    - echo "ADDITIONAL_ARTIFACTS=$ADDITIONAL_ARTIFACTS" >> metadata.env
    - echo "Build submitted - https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=$TASK_ID"
    - brew watch-task "$TASK_ID"
    - echo "Everything went well!"

.build_centos_stream_rpm:
  allow_failure: true
  timeout: 24h
  extends: [.with_artifacts, .with_retries]
  needs:
    - job: pipeline_init
      optional: true
      artifacts: true
    - job: check_tickets
      optional: true
      artifacts: true
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/distrobuildsync:8e34b66e"
  script:
    - !reference [.pick_build_options]
    - kinit -k -t "/opt/secrets/distrobaker.keytab" "distrobaker/distrobaker.osci.redhat.com@REDHAT.COM"
    - echo "Submitting koji $BUILD_TYPE-build!"
    - NS_COMPONENT="rpms/$CI_PROJECT_NAME"
    # scoped labels are mutually exclusive so no need for if-else
    - |
      if [ -n "$SIDE_TAG" ]; then
        TARGET=$SIDE_TAG
      else
        if [[ "$BUILD_TYPE" == "--draft" ]] ; then
          TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-draft
        else
          TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-candidate
        fi
      fi
    - echo "koji build target is $TARGET, build type is $BUILD_TYPE"
    - IFS=',' read -r -a labels <<< "$CI_MERGE_REQUEST_LABELS"
    - |
      for label in "${labels[@]}"
      do
          if [[ $label == target::* ]]; then
              RHEL_TARGET="${label#target::}"
              break
          fi
      done
    - |
      if [ -z "$RHEL_TARGET" ]; then
        echo "Warning: MR label not set, using default 'target::latest'"
      fi
    - |
      koji -p stream build $ARCH_OVERRIDE --fail-fast --nowait $BUILD_TYPE --custom-user-metadata "{\"rhel-target\": \"${RHEL_TARGET:-latest}\"}" $TARGET "git+https://gitlab.com/redhat/centos-stream/${NS_COMPONENT}.git#${ROG_COMMIT_SHA}" | tee koji_build.log
    - TASK_ID=$(cat koji_build.log | grep "^Created task:\ " | awk '{ print $3 }')
    - echo "KOJI_TASK_ID=$TASK_ID" >> metadata.env
    - echo "KOJI_BUILD_TARGET=$TARGET" >> metadata.env
    - koji -p stream watch-task "$TASK_ID"
    - echo "Everything went well!"

.generate_test_jobs:
  allow_failure: true
  extends: [.with_artifacts, .with_retries, .with_scripts]
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:590e052c"
  artifacts:
    paths:
      - test_runs.yml  # Generated dynamic pipeline
  needs:
    - job: build_rpm
      optional: true
      artifacts: true
  script:
    - echo "running python script that generates test jobs based on gating setup"
    - ./.rog-pipeline/./rog/scripts/generate-test-stage.py "rpms/$CI_PROJECT_NAME"  # Creates test_runs.yaml.
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - echo "KOJI_TASK_ID=$KOJI_TASK_ID" >> metadata.env
    - echo "ADDITIONAL_ARTIFACTS=$ADDITIONAL_ARTIFACTS" >> metadata.env

.trigger_tests:
  allow_failure: true
  needs:
    - job: generate_test_jobs
      artifacts: true
  trigger:
    include:
      - artifact: test_runs.yml
        job: generate_test_jobs
    strategy: depend
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID  # So the test pipelines know where to get metadata from
    SIDE_TAG: $SIDE_TAG
    KOJI_TASK_ID: $KOJI_TASK_ID
    ADDITIONAL_ARTIFACTS: $ADDITIONAL_ARTIFACTS

.sync_downstream:
  extends: [.with_retries]
  tags:
    - distrobaker
  image: images.paas.redhat.com/osci/base-minimal:901cda3c
  script:
    - NS_COMPONENT="rpms/$CI_PROJECT_NAME"
    - curl --fail-with-body -X POST "distrogitsync.osci.redhat.com/$NS_COMPONENT/branches/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"

.promote_build:
  image: "images.paas.redhat.com/osci/distrobuildsync:8e34b66e"
  tags:
    - distrobaker
  allow_failure: true
  extends: [.with_retries]
  needs:
    - job: sync_downstream
  script:
    - echo "Checking the build source commit hash is synced to dist-git"
    # Inspect dist-git branch to see if the new HEAD commit is the source commit for the build.
    - DOWNSTREAM_SHA=$(git ls-remote https://pkgs.devel.redhat.com/git/${NS_COMPONENT}/ "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" | cut -f1)
    - |
      if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        REF=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      else
        REF=$CI_COMMIT_SHA
      fi
    - |
      if [[ "$DOWNSTREAM_SHA" != "$REF" ]]; then
        echo "The source commit for this draft build has not been synced downstream - not promoting this build."
        exit 1
      fi
    - curl -s -o promote_build.py https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines/-/raw/main/rog/promote_build.py
    - kinit -k -t "/opt/secrets/distrobaker.keytab" "distrobaker/distrobaker.osci.redhat.com@REDHAT.COM"
    - echo "Using Brew API to promote RHEL draft build to production"
    - ./promote_build.py --rhel-or-stream rhel
    - |
      # Also promote the Koji (CS) build if we're in CS
      if [[ "$RHEL_OR_STREAM" == "centos-stream" ]]; then
        ./promote_build.py --rhel-or-stream centos-stream
      fi

.approve_mr:
  extends: [.with_retries]
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:590e052c"
  needs: [trigger_tests]
  # We need to allow this job to fail so that a human can approve instead of this job if required.
  allow_failure: true
  script:
    - !reference [.export_token]
    - export GLAB_CONFIG_DIR=/tmp
    - |
      PIPELINE_BRIDGES=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/bridges)
    - |
      TEST_STAGE_PIPELINE_ID=$(echo $PIPELINE_BRIDGES | jq -r '.[0].downstream_pipeline.id')
    - |
      TEST_STAGE_PIPELINE=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${TEST_STAGE_PIPELINE_ID})
    - |
      TEST_STAGE_PIPELINE_STATUS=$(echo $TEST_STAGE_PIPELINE | jq -r '.detailed_status.text')
    - |
      CHECK_TICKETS_STATUS=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs | jq -r '.[] | select(.name == "check_tickets") | .status')
    - |
      if [[ "$CHECK_TICKETS_STATUS" != "success" ]]; then
        glab mr note -m '<h4>check-tickets failed for this MR so it is not being approved, even if the tests have passed. Another run of check-tickets is made on-merge which will block unless this is fixed.</h4>' -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
      fi
      if [[ "$TEST_STAGE_PIPELINE_STATUS" == "Passed" ]] && [[ "$CHECK_TICKETS_STATUS" == "success" ]]; then
        echo "Test stage pipeline and check-tickets passed - approving the MR."
        glab mr note -m "<h4>Pipeline [$TEST_STAGE_PIPELINE_ID](https://gitlab.com/redhat/centos-stream/rpms/$CI_PROJECT_NAME/-/pipelines/$TEST_STAGE_PIPELINE_ID) passed.</h4>" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
        glab mr approve -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
      elif [[ "$TEST_STAGE_PIPELINE_STATUS" == "Warning" ]]; then
        echo "Test stage pipeline has warnings - one or more tests has failed - NOT approving the MR."
        TEST_STAGE_PIPELINE_JOBS=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${TEST_STAGE_PIPELINE_ID}/jobs)
        SUCCESSFUL_PIPELINE_JOBS=$(echo $TEST_STAGE_PIPELINE_JOBS | jq -r '[.[] | select(.status == "success") | {name: .name, web_url: .web_url}]')
        SUCCESS_STRING=$(echo $SUCCESSFUL_PIPELINE_JOBS | jq -r '.[] | "✅ \(.name): [Logs](\(.web_url))<br>"')
        FORMATTED_SUCCESS_STRING=$(echo "$SUCCESS_STRING" | tr -d '\n' | sed 's/<br>$//')
        FAILED_PIPELINE_JOBS=$(echo $TEST_STAGE_PIPELINE_JOBS | jq -r '[.[] | select(.status != "success") | {name: .name, web_url: .web_url}]')
        FAILED_STRING=$(echo $FAILED_PIPELINE_JOBS | jq -r '.[] | "❌ \(.name): [Logs](\(.web_url))<br>"')
        FORMATTED_FAILED_STRING=$(echo "$FAILED_STRING" | tr -d '\n' | sed 's/<br>$//')
        COMMENT_BODY="<h4>Pipeline [$TEST_STAGE_PIPELINE_ID](https://gitlab.com/redhat/centos-stream/rpms/$CI_PROJECT_NAME/-/pipelines/$TEST_STAGE_PIPELINE_ID) failed.</h4><details><summary>Show passing tests</summary>$FORMATTED_SUCCESS_STRING</summary></details>$FORMATTED_FAILED_STRING<br><br>Please review the test pipeline and resolve the issues. If you think the test failures are false positives please provide your justification in reply to this thread. Closing all unresolved threads will allow an approved maintainer to approve and merge this MR."
        curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --header "Content-Type: application/json" --data "{\"body\": \"${COMMENT_BODY}\"}" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/discussions
        exit 1
      fi
